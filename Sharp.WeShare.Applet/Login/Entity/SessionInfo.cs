﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.Applet.Login.Entity
{
    public class SessionInfo
    {
        public string SessionKey { get; set; }
        public string OpenId { get; set; }
    }
}
