﻿using System.Collections.Generic;
using Sharp.WeShare.Applet.TemplateNotice.Entity;
using Sharp.WeShare.Common;

namespace Sharp.WeShare.Applet.TemplateNotice
{
    /// <summary>
    /// 模板消息接口
    /// </summary>
    public class TemplateNoticeApi
    {
        /// <summary>
        /// 发送模板消息
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="touser"></param>
        /// <param name="templateId"></param>
        /// <param name="formId"></param>
        /// <param name="dataKeys"></param>
        /// <param name="emphasisKeyword"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static BaseRes Send(string accessToken, string touser, string templateId,string formId,  Dictionary<string, TemplateKey> dataKeys,string emphasisKeyword="",
        string page = "")
        {
            var turl =
            string.Format("https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token={0}", accessToken);
            var json = new
            {
                touser = touser,
                template_id = templateId,
                page = page,
                data = dataKeys,
                form_id= formId,
                emphasis_keyword= emphasisKeyword
            };
            return Utils.PostResult<BaseRes>(json, turl);
        }
    }
}