﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sharp.WeShare.Common;

namespace Sharp.WeShare.MP.MsgEntity
{
   public class LocationMenuEventMsg:BaseMenuEventMsg
    {
        /// <summary>
        /// 发送的位置信息
        /// </summary>
        [WxXml(HasChild = true)]
       public Sli SendLocationInfo { get; set; }
        public class  Sli
        {
            /// <summary>
            /// 朋友圈POI的名字，可能为空
            /// </summary>
            public string Poiname { get; set; } 
            public string Label { get; set; } 
            public string Scale { get; set; } 
            public string Location_Y { get; set; } 
            public string Location_X { get; set; }
        }
    }
}
