﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.MsgEntity
{
    /// <summary>
    /// 接收会员信息事件
    /// </summary>
    public class SubmitMemberUserInfoEventMsg : EventMsg
    {
        /// <summary>
        /// 卡券ID    
        /// </summary>
        public string CardId { get; set; }
        /// <summary>
        /// 卡券Code码
        /// </summary>
        public string UserCardCode { get; set; }
    }
}
