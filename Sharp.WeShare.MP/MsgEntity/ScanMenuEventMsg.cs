﻿using Sharp.WeShare.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.MsgEntity
{
    public class ScanMenuEventMsg : BaseMenuEventMsg
    {
        public class Sci
        {
            /// <summary>
            /// 扫描类型，一般是qrcode
            /// </summary>
            public string ScanType { get; set; }
            /// <summary>
            /// 扫描结果，即二维码对应的字符串信息
            /// </summary>
            public string ScanResult { get; set; }
        }
        [WxXml(HasChild =true)]
        public Sci ScanCodeInfo { get; set; }

    }

}
