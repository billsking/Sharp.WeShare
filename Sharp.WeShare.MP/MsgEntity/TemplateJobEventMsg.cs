﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.MsgEntity
{
    /// <summary>
    /// 模板消息事件推送
    /// </summary>
    public class TemplateJobEventMsg: EventMsg
    {
        public string MsgID { get; set; }
        public string Status { get; set; }
    }
}
