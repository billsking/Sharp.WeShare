﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.MsgEntity
{
    public interface IMsgHandler
    {
        /// <summary>
        /// 文本消息回调
        /// </summary>
        /// <param name="msg"></param>
        void TextHandler(TextMsg msg);
        /// <summary>
        /// 图片消息回调
        /// </summary>
        /// <param name="msg"></param>
        void ImageHandler(ImgMsg msg);
        /// <summary>
        /// 语音消息回调
        /// </summary>
        /// <param name="msg"></param>
        void VoiceHandler(VoiceMsg msg);
        /// <summary>
        /// 视频消息回调
        /// </summary>
        /// <param name="msg"></param>
        void VideoHandler(VideoMsg msg);
        /// <summary>
        /// 位置消息回调
        /// </summary>
        /// <param name="msg"></param>
        void LocationHandler(LocationMsg msg);
        /// <summary>
        /// 链接消息回调
        /// </summary>
        /// <param name="msg"></param>
        void LinkHandler(LinkMsg msg);
        /// <summary>
        /// 扫描二维码事件回调
        /// </summary>
        /// <param name="msg"></param>
        void ScanHandler(ScanQrEventMsg msg);
        /// <summary>
        /// 关注事件回调
        /// </summary>
        /// <param name="msg"></param>
        void SubscribeHandler(SubscribeEventMsg msg);
        /// <summary>
        /// 取消关注事件回调
        /// </summary>
        /// <param name="msg"></param>
        void UnSubscribeHandler(EventMsg msg);
        /// <summary>
        /// 自定义菜单跳转回调
        /// </summary>
        /// <param name="msg"></param>
        void MenuViewHandler(ViewEventMsg msg);
        /// <summary>
        /// 获取用户地理位置回调
        /// </summary>
        /// <param name="msg"></param>
        void LocationEventHandler(LocationEventMsg msg);

        void CommonHandler(BaseMsg msg);
        /// <summary>
        /// 认证通知
        /// </summary>
        /// <param name="msg"></param>
        void VerifyHandler(VerifyEventMsg msg);
        /// <summary>
        /// 卡券内快捷支付事件
        /// </summary>
        /// <param name="msg"></param>
        void UserPayHandler(UserPayEventMsg msg);
        /// <summary>
        /// 用户领取卡券事件
        /// </summary>
        /// <param name="msg"></param>
        void UserGetCardHandler(UserGetCardEventMsg msg);
        /// <summary>
        /// 门店审核事件
        /// </summary>
        /// <param name="msg"></param>
        void PoiNotifyHandler(PoiNotifyEventMsg msg);
        /// <summary>
        /// 接收会员信息事件
        /// </summary>
        /// <param name="msg"></param>
        void SubmitMemberUserInfoHandler(SubmitMemberUserInfoEventMsg msg);
        /// <summary>
        /// click菜单事件
        /// </summary>
        /// <param name="msg"></param>
        void MenuClickHandler(BaseMenuEventMsg msg);
        /// <summary>
        /// 自定义菜单按钮扫描二维码事件.完成扫码操作后显示扫描结果（如果是URL，将进入URL），且会将扫码的结果传给开发者，开发者可以下发消息。
        /// </summary>
        /// <param name="msg"></param>
        void MenuScanPushHandler(ScanMenuEventMsg msg);
        /// <summary>
        /// 定义菜单按钮扫描二维码事件.扫码推事件且弹出“消息接收中”提示框用户点击按钮后，微信客户端将调起扫一扫工具，完成扫码操作后，将扫码的结果传给开发者，同时收起扫一扫工具，然后弹出“消息接收中”提示框，随后可能会收到开发者下发的消息。
        /// </summary>
        /// <param name="msg"></param>
        void MenuScanWaitHandler(ScanMenuEventMsg msg);
        /// <summary>
        /// 菜单发图片事件
        /// </summary>
        /// <param name="msg"></param>
        void MenuPicHandler(PicMenuEventMsg msg);
        /// <summary>
        /// 自定义菜单发送地理位置事件
        /// </summary>
        /// <param name="msg"></param>
        void MenuLocationHandler(LocationMenuEventMsg msg);
        /// <summary>
        /// 事件推送群发结果
        /// </summary>
        /// <param name="msg"></param>
        void GroupSendJobHandler(GroupSendJobEventMsg msg);
        /// <summary>
        /// 模板消息事件推送
        /// </summary>
        /// <param name="msg"></param>
        void TemplateJobEventHandler(TemplateJobEventMsg msg);
    }
}

