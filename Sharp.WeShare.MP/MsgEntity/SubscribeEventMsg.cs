﻿namespace Sharp.WeShare.MP.MsgEntity
{
    /// <summary>
    /// 关注事件
    /// </summary>
    public class SubscribeEventMsg : EventMsg
    {
        private string _eventkey;
        /// <summary>
        /// 事件key值
        /// </summary>
        public string EventKey
        {
            get
            {
                if (!string.IsNullOrEmpty(_eventkey))
                    //扫码二维码事件时，获取真实的场景值
                    return _eventkey.Replace("qrscene_", "");
                return "";
            }
            set { _eventkey = value; }
        }
        /// <summary>
        /// 二维码的ticket
        /// </summary>
        public string Ticket { get; set; }
    }

}
