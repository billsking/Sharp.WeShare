﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Sharp.WeShare.Common;

namespace Sharp.WeShare.MP.MsgEntity
{
    /// <summary>
    /// 文本实体
    /// </summary>
    public class TextMsg : BaseMsg
    {
        /// <summary>
        /// 消息内容
        /// </summary>
        public string Content { get; set; }
    }
}
