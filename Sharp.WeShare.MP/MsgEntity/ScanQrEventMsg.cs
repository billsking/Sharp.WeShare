﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.MsgEntity
{
    public class ScanQrEventMsg : EventMsg
    {
        /// <summary>
        /// 二维码的ticket，可用来换取二维码图片
        /// </summary>
        public string Ticket { get; set; }

        public string EventKey { get; set; }
    }

}
