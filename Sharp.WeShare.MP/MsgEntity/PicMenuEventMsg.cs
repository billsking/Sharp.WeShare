﻿using Sharp.WeShare.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.MsgEntity
{
    public class PicMenuEventMsg : BaseMenuEventMsg
    {
       public class Spi
        {
            public int Count { get; set; }
            public class Pl
            {
                public class I
                {
                    public string PicMd5Sum { get; set; }
                }
                [WxXml(IsList =true)]
                public List<I> item { get; set; }
            }
            [WxXml(HasChild =true)]
            public Pl PicList { get; set; }
        }
        [WxXml(HasChild =true)]
        public Spi SendPicsInfo { get; set; }
    }

}
