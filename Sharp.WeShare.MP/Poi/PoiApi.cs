﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Sharp.WeShare.Common;
using Sharp.WeShare.MP.Poi.Entity;

namespace Sharp.WeShare.MP.Poi
{
    /// <summary>
    /// 门店管理接口
    /// </summary>
    public class PoiApi
    {
        /// <summary>
        /// 创建门店
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="info">门店信息</param>
        /// <returns></returns>
        public static BaseRes Add(string accessToken, BaseInfo info)
        {
            var url = string.Format("http://api.weixin.qq.com/cgi-bin/poi/addpoi?access_token={0}", accessToken);
            var obj = new { business = new { base_info = info } };
            return Utils.PostResult<BaseRes>(obj, url);
        }
        /// <summary>
        /// 获取门店信息
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="poiId"></param>
        /// <returns></returns>
        public static PoiInfo GetInfo(string accessToken, string poiId)
        {
            var url = string.Format("http://api.weixin.qq.com/cgi-bin/poi/getpoi?access_token={0}",accessToken);
            var data = new {poi_id = poiId};
            return Utils.PostResult<PoiInfo>(data, url);
        }
        /// <summary>
        /// 获取门店列表
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="begin">开始位置，0 即为从第一条开始查询</param>
        /// <param name="limit">返回数据条数，最大允许50，默认为20</param>
        /// <returns></returns>
        public static Pois GetInfo(string accessToken, int begin,int limit)
        {
            var url = string.Format("https://api.weixin.qq.com/cgi-bin/poi/getpoilist?access_token={0}", accessToken);
            var data = new { begin = begin,limit=limit };
            return Utils.PostResult<Pois>(data, url);
        }
        /// <summary>
        /// 修改门店信息。若有填写内容则为覆盖更新，若无内容则视为不修改，维持原有内容。
        ///  photo_list 字段为全列表覆盖，若需要增加图片，需将之前图片同样放入list 中，在其后增加新增图片。
        /// 如：已有A、B、C 三张图片，又要增加D、E 两张图，则需要调用该接口，photo_list 传入A、B、C、D、E 五张图片的链接。
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static BaseRes Update(string accessToken, UpdateParam param)
        {
            var url = string.Format("http://api.weixin.qq.com/cgi-bin/poi/addpoi?access_token={0}", accessToken);
            var obj = new { business = new { base_info = param } };
            return Utils.PostResult<BaseRes>(obj, url);
        }
        /// <summary>
        /// 删除门店
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="poiId"></param>
        /// <returns></returns>
        public static BaseRes Delete(string accessToken,string poiId)
        {
            var url = string.Format("http://api.weixin.qq.com/cgi-bin/poi/delpoi?access_token={0}", accessToken);
            var obj = new { poi_id= poiId };
            return Utils.PostResult<BaseRes>(obj, url);
        }
        /// <summary>
        /// 获取门店分类
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static string[] GetCatagory(string accessToken)
        {
            var url = string.Format("http://api.weixin.qq.com/cgi-bin/poi/getwxcategory?access_token={0}",accessToken);
            var jobj =  Utils.GetResult<JObject>(url);
            return jobj["category_list"].ToObject<string[]>();
        }
        
    }
}