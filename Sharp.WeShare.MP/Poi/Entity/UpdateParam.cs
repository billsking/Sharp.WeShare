﻿using System.Collections.Generic;

namespace Sharp.WeShare.MP.Poi.Entity
{
    public class UpdateParam
    {
        /// <summary>
        /// 商户自定义id
        /// </summary>
        public string sid { get; set; }
        /// <summary>
        ///  必填。门店的电话（纯数字，区号、分机号均由“-”隔开）
        /// </summary>
        public string telephone { get; set; }
        #region 门店服务信息字段。非必填
        /// <summary>
        /// 图片列表，url 形式，可以有多张图片，尺寸为640 px *340px。
        /// </summary>
        public List<PhotoUrl> photo_list { get; set; }
        /// <summary>
        /// 推荐品，餐厅可为推荐菜；酒店为推荐套房；景点为推荐游玩景点等，针对自己行业的推荐内容
        /// </summary>
        public string recommend { get; set; }
        /// <summary>
        /// 特色服务，如免费 Wi-Fi、免费停车、送货上门等商户能提供的特色功能或服务
        /// </summary>
        public string special { get; set; }
        /// <summary>
        /// 商户简介，主要介绍商户信息等
        /// </summary>
        public string introduction { get; set; }
        /// <summary>
        /// 营业时间，24 小时制表示，用“-”连接，如8:00-20:00
        /// </summary>
        public string open_time { get; set; }
        /// <summary>
        /// 人均价格，大于 0 的整数
        /// </summary>
        public int avg_price { get; set; }
        public class PhotoUrl
        {
            public string photo_url { get; set; }
        }
        #endregion

    }
}