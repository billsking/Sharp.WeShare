﻿namespace Sharp.WeShare.MP.Poi.Entity
{
    /// <summary>
    /// 查询门店时，返回的实体
    /// </summary>
    public class PoiInfo:BaseRes
    {
        /// <summary>
        ///门店信息
        /// </summary>
        public class Business
        {
            /// <summary>
            /// 门店基本信息
            /// </summary>
            public BaseInfo base_info { get; set; } 
        }
        /// <summary>
        /// 门店信息
        /// </summary>
        public Business business { get; set; }
         
    }
}