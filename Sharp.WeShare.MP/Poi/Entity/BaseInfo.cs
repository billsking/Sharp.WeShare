﻿using System.Collections.Generic;

namespace Sharp.WeShare.MP.Poi.Entity
{
    public class BaseInfo
    {
        /// <summary>
        /// 商户自定义id
        /// </summary>
        public string sid { get; set; }
        /// <summary>
        /// 必填。门店名称.仅为商户名，如：国美.不应包含地区、店号等信息，错误示例：北京国美
        /// </summary>
        public string business_name { get; set; }
        /// <summary>
        ///  必填。分店名称（不应包含地区信息、不应与门店名重复，错误示例：北京王府井店）
        /// </summary>
        public string branch_name { get; set; }
        /// <summary>
        /// 必填。 门店所在的省份（直辖市填城市名，如：北京市）
        /// </summary>
        public string province { get; set; }
        /// <summary>
        /// 必填。 门店所在的城市
        /// </summary>
        public string city { get; set; }
        /// <summary>
        ///  必填。门店所在地区
        /// </summary>
        public string district { get; set; }
        /// <summary>
        ///  必填。门店所在的详细街道地址（不要填写省市信息）
        /// </summary>
        public string address { get; set; }
        /// <summary>
        ///  必填。门店的电话（纯数字，区号、分机号均由“-”隔开）
        /// </summary>
        public string telephone { get; set; }
        /// <summary>
        ///  必填。门店的类型，不同级分类用“,”隔开，如：美食,川菜,火锅
        /// </summary>
        public string categories { get; set; }
        /// <summary>
        ///  必填。坐标类型，1 为火星坐标（目前只能选 1） 
        /// </summary>
        public int offset_type { get; set; }
        /// <summary>
        ///  必填。门店所在地理位置的经度 
        /// </summary>
        public string longitude { get; set; }
        /// <summary>
        ///  必填。门店所在地理位置的纬度
        /// </summary>
        public string latitude { get; set; }
        #region 门店服务信息字段。非必填
        /// <summary>
        /// 图片列表，url 形式，可以有多张图片，尺寸为640 px *340px。
        /// </summary>
        public List<PhotoUrl> photo_list { get; set; }
        /// <summary>
        /// 推荐品，餐厅可为推荐菜；酒店为推荐套房；景点为推荐游玩景点等，针对自己行业的推荐内容
        /// </summary>
        public string recommend { get; set; }
        /// <summary>
        /// 特色服务，如免费 Wi-Fi、免费停车、送货上门等商户能提供的特色功能或服务
        /// </summary>
        public string special { get; set; }
        /// <summary>
        /// 商户简介，主要介绍商户信息等
        /// </summary>
        public string introduction { get; set; }
        /// <summary>
        /// 营业时间，24 小时制表示，用“-”连接，如8:00-20:00
        /// </summary>
        public string open_time { get; set; }
        /// <summary>
        /// 人均价格，大于 0 的整数
        /// </summary>
        public int avg_price { get; set; }
        public class PhotoUrl
        {
            public string photo_url { get; set; }
        }
        #endregion

        #region 查询门店信息时专用
        /// <summary>
        /// 门店是否可用状态。1 表示系统错误、2 表示审核中、3 审核通过、4 审核驳回。当该字段为1、2、4 状态时，poi_id 为空
        /// </summary>
        public int available_state { get; set; }
        /// <summary>
        /// 扩展字段是否正在更新中。1 表示扩展字段正在更新中，尚未生效，不允许再次更新； 0 表示扩展字段没有在更新中或更新已生效，可以再次更新
        /// </summary>
        public int update_status { get; set; }
        #endregion
    }
}