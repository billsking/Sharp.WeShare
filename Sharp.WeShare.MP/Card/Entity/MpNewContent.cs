﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
   public class MpNewContent:BaseRes
    {
        /// <summary>
        /// 返回一段html代码，可以直接嵌入到图文消息的正文里。即可以把这段代码嵌入到上传图文消息素材接口中的content字段里。
        /// </summary>
        public string content { get; set; }
    }
}
