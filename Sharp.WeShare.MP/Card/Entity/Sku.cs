﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    public class Sku
    {
        /// <summary>
        /// 卡券库存的数量（不支持填写0或无限大）创建预存模式卡券时需填0
        /// </summary>
        public int quantity { get; set; }
    }
}
