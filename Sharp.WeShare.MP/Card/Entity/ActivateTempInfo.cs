﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Sharp.WeShare.MP.Card.Entity.UserInfoRes;

namespace Sharp.WeShare.MP.Card.Entity
{
    public class ActivateTempInfo : BaseRes
    {
        /// <summary>
        /// 用户提交资料
        /// </summary>
        public UserInfo info { get; set; }
    }
}
