﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 兑换券
    /// </summary>
    public class Gift:BaseCard
    {
        public CardInfo gift { get; set; }
        public Gift()
        {
            this.card_type = "GIFT";
        }
    }
}
