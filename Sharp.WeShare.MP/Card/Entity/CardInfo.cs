﻿using Sharp.WeShare.MP.EnumKey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 所有卡券的基类
    /// </summary>
    public class CardInfo
    {
        /// <summary>
        /// 基础字段
        /// </summary>
        public BaseInfo base_info { get; set; }
        /// <summary>
        /// 高级字段，可选字段，朋友的券必填
        /// </summary>
        public AdvancedInfo advanced_info { get; set; }
        /// <summary>
        /// 代金券专用字段，表示起用金额（单位为分）,如果无起用门槛则填0。
        /// </summary>
        public int? least_cost { get; set; }
        /// <summary>
        ///代金券专用字段，表示减免金额。（单位为分）
        /// </summary>
        public int? reduce_cost { get; set; }
        /// <summary>
        ///折扣券和会员卡专用字段， 表示打折额度（百分比）。填30就是七折。
        /// </summary>
        public int? discount { get; set; }

        /// <summary>
        /// 优惠券专用字段，填写优惠详情。
        /// </summary>
        public string default_detail { get; set; }

        /// <summary>
        /// 兑换券专用字段，填写兑换内容的名称。
        /// </summary>
        public string gift { get; set; }
        /// <summary>
        /// 兑换券专用字段，兑换券兑换商品名字，限6个汉字
        /// </summary>
        public string gift_name { get; set; }
        /// <summary>
        /// 兑换券兑换商品数目，限三位数字，非必填
        /// </summary>
        public int? gift_num { get; set; }
        /// <summary>
        /// 兑换券兑换商品的数量单位，限两个汉字
        /// </summary>
        public string gift_unit { get; set; }
        /// <summary>
        /// 团购券专用字段。
        /// </summary>
        public string deal_detail { get; set; }
        #region 会员卡专属字段
        /// <summary>
        /// 会员卡背景图。1000像素*600像素以下
        /// </summary>
        public string background_pic_url { get; set; }
        /// <summary>
        /// 会员卡特权说明。        
        /// </summary>
        public string prerogative { get; set; }
        /// <summary>
        /// 设置为true时用户领取会员卡后系统自动将其激活，无需调用激活接口.传入自动激活字段auto_activate之后，一键开卡设置和接口激活设置的激活url均不再显示，用户领取卡片之后，系统自动帮用户激活，积分、储值等自定义显示信息均为0
        /// </summary>
        public bool? auto_activate { get; set; }
        /// <summary>
        /// 是否支持一键激活
        /// </summary>
        public bool? wx_activate { get; set; }
        /// <summary>
        /// 是否支持跳转型一键激活
        /// </summary>
        public bool? wx_activate_after_submit { get; set; }
        /// <summary>
        /// 跳转型一键激活跳转的地址链接           
        /// </summary>
        public string wx_activate_after_submit_url { get; set; }
        public bool? supply_bonus { get; set; }
        public string bonus_url { get; set; }
        public bool? supply_balance { get; set; }
        public string balance_url { get; set; }
        public CustomField custom_field1 { get; set; }
        public CustomField custom_field2 { get; set; }
        public CustomField custom_field3 { get; set; }
        /// <summary>
        /// 会员卡自定义类目
        /// </summary>
        public class CustomField
        {
            public MemberCardType name_type { get; set; }
            /// <summary>
            /// 点击类目跳转外链url            
            /// </summary>
            public string url { get; set; }
        }

        public string bonus_cleared { get; set; }
        public string bonus_rules { get; set; }
        public string balance_rules { get; set; }
        public string activate_url { get; set; }
        /// <summary>
        /// 自定义会员信息类目，会员卡激活后显示
        /// </summary>
        public class CustomCell
        {
            /// <summary>
            /// 入口名称。   
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 入口右侧提示语，6个汉字内。            
            /// </summary>
            public string tips { get; set; }
            /// <summary>
            /// 入口跳转链接。
            /// </summary>
            public string url { get; set; }
        }
        /// <summary>
        /// 自定义会员信息类目，会员卡激活后显示。   
        /// </summary>
        public CustomCell custom_cell1 { get; set; }

        public BonusRule bonus_rule { get; set; }
        /// <summary>
        /// 积分规则。用于微信买单功能。           
        /// </summary>
        public class BonusRule
        {
            /// <summary>
            /// 消费金额。以分为单位。            
            /// </summary>
            public int cost_money_unit { get; set; }
            /// <summary>
            /// 对应增加的积分。     
            /// </summary>
            public int increase_bonus { get; set; }
            /// <summary>
            /// 用户单次可获取的积分上限。        
            /// </summary>
            public int max_increase_bonus { get; set; }
            /// <summary>
            /// 初始设置积分。          
            /// </summary>
            public int init_increase_bonus { get; set; }
            /// <summary>
            /// 每使用5积分。
            /// </summary>
            public int cost_bonus_unit { get; set; }
            /// <summary>
            /// 抵扣xx元，（这里以分为单位）            
            /// </summary>
            public int reduce_money { get; set; }
            /// <summary>
            /// 抵扣条件，满xx元（这里以分为单位）可用。 
            /// </summary>
            public int least_money_to_use_bonus { get; set; }
            /// <summary>
            /// 抵扣条件，单笔最多使用xx积分。        
            /// </summary>
            public int max_reduce_bonus { get; set; }
        }
        #endregion

    }
}
