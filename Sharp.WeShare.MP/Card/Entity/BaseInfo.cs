﻿using Sharp.WeShare.MP.EnumKey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 卡券基本信息
    /// </summary>
    public class BaseInfo
    {
        /// <summary>
        /// 卡券的商户 LOGO，尺寸为300*300，必填
        /// 此字段可更新
        /// </summary>
        public string logo_url { get; set; }
        /// <summary>
        /// code 码展示类型。必填
        /// 此字段可更新
        /// </summary>
        public CodeType code_type { get; set; }
        /// <summary>
        /// 商户名字,字数上限为 12 个汉字（填写直接提供服务的商户名，第三方商户名填写在 source 字段） 必填
        /// </summary>
        public string brand_name { get; set; }
        /// <summary>
        /// 券名，字数上限为 9 个汉字(建议涵盖卡券属性、服务及金额) 必填，朋友的券不可用
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 券名的副标题， 字数上限为 18 个汉字，朋友的券不可用
        /// </summary>
        public string sub_title { get; set; }
        /// <summary>
        /// 券颜色 必填
        /// </summary>
        public string color { get; set; }
        /// <summary>
        /// 使用提醒，字数上限为 12 个汉字（一句话描述，展示在首页，示例：请出示二维码核销卡券） 必填
        /// 此字段可更新
        /// </summary>
        public string notice { get; set; }
        /// <summary>
        /// 使用说明。长文本描述，可以分行，上限为 1000 个汉字 必填
        /// 此字段可更新
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// 使用日期，有效期的信息 必填
        /// 此字段可更新
        /// </summary>
        public DateInfo date_info { get; set; }
        /// <summary>
        /// 门店位置 ID。商户需在 mp 平台上录入门店信息或调用批量导入门店信息接口获取门店位置 ID，朋友的券须至少传入一个可用poi_id,否则报错
        /// 此字段可更新
        /// </summary>
        public string[] location_id_list { get; set; }
        /// <summary>
        /// 设置本卡券支持全部门店，与location_id_list互斥，朋友的券不可用
        /// 此字段可更新
        /// </summary>
        public bool? use_all_locations { get; set; }

        /// <summary>
        /// 是否自定义 code 码。填写 true或 false。不填则代表默认为 false。该字段需单独申请权限， 朋友的券不可用
        /// </summary>
        public bool? use_custom_code { get; set; }
        /// <summary>
        /// 是否指定用户领取，默认为否，创建朋友的券时，此字段无效
        /// </summary>
        public bool? bind_openid { get; set; }
        /// <summary>
        /// 领取卡券原生页面是否可分享。默认为true,若创建朋友共享券此处应填入false,不可为空
        /// 此字段可更新
        /// </summary>
        public bool can_share { get; set; }
        /// <summary>
        /// 是否可转增。默认为true,若创建朋友共享券此处应填入false,不可为空
        /// 此字段可更新
        /// </summary>
        public bool can_give_friend { get; set; }
        /// <summary>
        /// 每人最大领取次数。若不填写，则默认等于库存
        /// 此字段可更新
        /// </summary>
        public int get_limit { get; set; }
        /// <summary>
        /// 客服电话
        /// 此字段可更新
        /// </summary>
        public string service_phone { get; set; }
        /// <summary>
        /// 第三方来源名，例如同程旅游、格瓦拉，朋友的券此参数不可用
        /// </summary>
        public string source { get; set; }
        /// <summary>
        /// 商户自定义入口名称，custom_url 字段共同使用，长度限制在 5 个汉字内
        /// 此字段可更新
        /// </summary>
        public string custom_url_name { get; set; }
        /// <summary>
        /// 卡券顶部居中的按钮，仅在卡券状态正常(可以核销)时显示。 若要使用快速买单和自助核销功能，此参数为空
        /// 此字段可更新
        /// </summary>
        public string center_title { get; set; }
        /// <summary>
        /// 显示在入口下方的提示语，仅在卡券状态正常(可以核销)时显示。若要使用快速买单和自助核销功能，此参数为空
        /// 此字段可更新
        /// </summary>
        public string center_sub_title { get; set; }
        /// <summary>
        /// 顶部居中的url，仅在卡券状态正常(可以核销)时显示。若要使用快速买单和自助核销功能，此参数为空
        /// 此字段可更新
        /// </summary>
        public string center_url { get; set; }
        /// <summary>
        /// 商户自定义入口跳转外链的地址链接,跳转页面内容需与自定义cell名称保持匹配
        /// 此字段可更新
        /// </summary>
        public string custom_url { get; set; }
        /// <summary>
        /// 显示在入口右侧的 tips，长度限制在6个汉字内
        /// 此字段可更新
        /// </summary>
        public string custom_url_sub_title { get; set; }
        /// <summary>
        /// 营销场景的自定义入口
        /// 此字段可更新
        /// </summary>
        public string promotion_url_name { get; set; }
        /// <summary>
        /// 入口跳转外链的地址链接
        /// 此字段可更新
        /// </summary>
        public string promotion_url { get; set; }
        /// <summary>
        /// 显示在入口右侧的tips，长度限制在6个汉字内
        /// 此字段可更新
        /// </summary>
        public string promotion_url_sub_title { get; set; }
        /// <summary>
        /// sku， 必填，朋友的券为空
        /// </summary>
        public Sku sku { get; set; }
        /// <summary>
        /// 填入该字段后，自定义code卡券方可进行导入code并投放的动作。值为：GET_CUSTOM_CODE_MODE_DEPOSIT。
        /// 创建/更新填入get_custom_code_mode时，须检查库存数与已经导入code数目的关系，当导入code的数目小于库存数时，会报错。
        /// </summary>
        public string get_custom_code_mode { get; set; }
        /// <summary>
        /// 1.对于部分有特殊权限的商家，查询卡券详情得到的返回可能含特殊接口的字段。
        /// 2.由于卡券字段会持续更新，实际返回字段包含但不限于文档中的字段，建议开发者开发时对于不理解的字段不做处理，以免出错。
        /// </summary>
        public CardStatus status { get; set; }
        /// <summary>
        /// 填写true为用户点击进入会员卡时推送事件，默认为false
        /// </summary>
        public bool? need_push_on_view { get; set; }
    }
}
