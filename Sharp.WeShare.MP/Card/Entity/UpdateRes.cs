﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 更新卡券信息
    /// </summary>
    public class UpdateRes : BaseRes
    {
        /// <summary>
        /// 是否提交审核，false为修改后不会重新提审，true为修改字段后重新提审，该卡券的状态变为审核中。
        /// </summary>
        public bool send_check { get; set; }
    }
}
