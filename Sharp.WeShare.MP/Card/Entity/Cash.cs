﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 代金券
    /// </summary>
    public class Cash:BaseCard
    {
        public CardInfo cash { get; set; }
        public Cash()
        {
            this.card_type = "CASH";
        }
    }
}
