﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 团购券
    /// </summary>
    public class Groupon:BaseCard
    {
        public CardInfo groupon { get; set; }
        public Groupon()
        {
            this.card_type = "GROUPON";
        }
    }
}
