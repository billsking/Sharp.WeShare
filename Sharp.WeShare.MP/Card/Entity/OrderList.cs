﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 订单列表接口返回的实体类
    /// </summary>
    public class OrderList:BaseRes
    {
        /// <summary>
        /// 符合条件的订单总数量
        /// </summary>
        public int total_num { get; set; }
        /// <summary>
        /// 订单详情列表
        /// </summary>
        public List<OrderInfo> order_list { get; set; }
    }
}
