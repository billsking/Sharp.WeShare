﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// code解码实体
    /// </summary>
   public class DecryptCodeRes:BaseRes
    {
        /// <summary>
        /// 解码后的code
        /// </summary>
        public string code { get; set; }
    }
}
