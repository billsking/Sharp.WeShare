﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 核销卡券的响应实体类
    /// </summary>
    public class ConsumeRes : BaseRes
    {
        /// <summary>
        /// 用户在该公众号内的唯一身份标识。
        /// </summary>
        public string openid { get; set; }
        public Card card { get; set; }
        public class Card
        {
            /// <summary>
            /// 卡券ID。
            /// </summary>
            public string card_id { get; set; }
        }
    }
}
