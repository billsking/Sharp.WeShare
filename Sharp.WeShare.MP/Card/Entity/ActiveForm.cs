﻿using Sharp.WeShare.MP.EnumKey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 会员卡激活字段
    /// </summary>
    public class ActiveForm
    {
        public string card_id { get; set; }
        /// <summary>
        /// 会员卡激活时的必填选项。 
        /// </summary>
        public Forms required_form { get; set; }
        public Forms optional_form { get; set; }
        public NameUrl service_statement { get; set; }
        public NameUrl bind_old_card { get; set; }
        /// <summary>
        /// 会员卡激活时的必填选项。 
        /// </summary>
        public class Forms
        {
            /// <summary>
            /// 当前结构（required_form或者optional_form ）内的字段是否允许用户激活后再次修改，商户设置为true时，需要接收相应事件通知处理修改事件
            /// </summary>
            public bool? can_modify { get; set; }
            public List<RichField> rich_field_list { get; set; }
            public List<CommonField> common_field_id_list { get; set; }
            /// <summary>
            /// 自定义选项名称。
            /// </summary>
            public List<string> custom_field_list { get; set; }
        }
        public class RichField
        {
            /// <summary>
            /// 文本类型
            /// </summary>
            public RichFieldType type { get; set; }
            /// <summary>
            /// 字段名        
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 选择项
            /// </summary>
            public string values { get; set; }
        }
        public class NameUrl
        {
            /// <summary>
            /// 会员声明字段名称
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 自定义url
            /// </summary>
            public string url { get; set; }
        }
    }
}
