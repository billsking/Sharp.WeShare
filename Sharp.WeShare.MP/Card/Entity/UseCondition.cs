﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    public class UseCondition
    {
        /// <summary>
        /// 指定可用的商品类目，仅用于代金券类型，填入后将在券面拼写适用于xxx
        /// </summary>
        public string accept_category { get; set; }
        /// <summary>
        /// 指定可用的商品类目，仅用于代金券类型，填入后将在券面拼写不适用于xxxx
        /// </summary>
        public string reject_category { get; set; }
        /// <summary>
        /// 满减门槛字段，可用于兑换券和代金券，填入后将在券面拼写消费满xx元可用。
        /// </summary>
        public string least_cost { get; set; }
        /// <summary>
        /// 购买xx可用类型门槛，仅用于兑换，填入后自动拼写购买xxx可用。
        /// </summary>
        public bool object_use_for { get; set; }
        /// <summary>
        /// 不可以与其他类型共享门槛
        ///，填写false时系统将在使用须知里
        ///拼写“不可与其他优惠共享”，
        ///填写true时系统将在使用须知里
        ///拼写“可与其他优惠共享”，
        ///默认为true
        /// </summary>
        public bool can_use_with_other_discount { get; set; }
    }
}
