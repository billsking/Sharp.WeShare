﻿using Sharp.WeShare.MP.EnumKey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    public class UserInfoRes : BaseRes
    {
        public string openid { get; set; }
        public string nickname { get; set; }
        public string membership_number { get; set; }
        public int bonus { get; set; }
        public string sex { get; set; }
        public UserInfo user_info { get; set; }
        public CodeCardStatus user_card_status { get; set; }
        /// <summary>
        /// 该卡是否已经被激活，true表示已经被激活，false表示未被激活
        /// </summary>
        public bool has_active { get; set; }
        
        public class UserInfo
        {
            public List<NameValue> common_field_list { get; set; }
            public List<string> custom_field_list { get; set; }
            public class NameValue
            {
                public string name { get; set; }
                public string value { get; set; }
            }
        }
    }
}
