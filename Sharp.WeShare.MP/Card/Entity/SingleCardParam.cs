﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    ///生成二维码时，领取单张卡券的参数
    /// </summary>
    public class SingleCardParam
    {
        /// <summary>
        /// 卡券ID。
        /// </summary>
        public string card_id { get; set; }
        /// <summary>
        /// 卡券Code码,use_custom_code字段为true的卡券必须填写，非自定义code和导入code模式的卡券不必填写。
        /// </summary>
        public string code { get; set; }
        /// <summary>
        /// 指定领取者的openid，只有该用户能领取。bind_openid字段为true的卡券必须填写，非指定openid不必填写。
        /// </summary>
        public string openid { get; set; }
        /// <summary>
        /// 指定下发二维码，生成的二维码随机分配一个code，领取后不可再次扫描。填写true或false。默认false，注意填写该字段时，卡券须通过审核且库存不为0。
        /// </summary>
        public bool is_unique_code { get; set; }
        /// <summary>
        /// 对于会员卡的二维码，用户每次扫码打开会员卡后点击任何url，会将该值拼入url中，方便开发者定位扫码来源
        /// </summary>
        public string outer_str { get; set; }
    }
}
