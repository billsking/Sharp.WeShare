﻿using Sharp.WeShare.Common;
using Sharp.WeShare.MP.EnumKey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 卡券信息
    /// </summary>
    public class CodeInfoRes : BaseRes
    {
        public Card card { get; set; }
        /// <summary>
        /// 用户openid
        /// </summary>
        public string openid { get; set; }
        /// <summary>
        /// 是否可以核销，true为可以核销，false为不可核销
        /// </summary>
        public bool? can_consume { get; set; }
        /// <summary>
        /// 当前code对应卡券的状态
        /// </summary>
        public CodeCardStatus? user_card_status { get; set; }
        public class Card
        {
            /// <summary>
            /// 卡券ID
            /// </summary>
            public string card_id { get; set; }
            /// <summary>
            /// 起始使用时间
            /// </summary>
            public int begin_time { get; set; }
            public DateTime BeginTime
            {
                get { return Utils.UnixTimeToTime(begin_time.ToString()); }
            }
            public DateTime EndTime
            {
                get { return Utils.UnixTimeToTime(end_time.ToString()); }
            }
            /// <summary>
            /// 结束时间
            /// </summary>
            public int end_time { get; set; }
        }
    }
}
