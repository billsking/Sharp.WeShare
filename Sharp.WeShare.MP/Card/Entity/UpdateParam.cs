﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 更新会员的参数
    /// </summary>
    public class UpdateParam
    {
        /// <summary>
        /// 卡券Code码。 
        /// </summary>
        public string code { get; set; }
        /// <summary>
        /// 卡券ID。    
        /// </summary>
        public string card_id { get; set; }
        /// <summary>
        /// 支持商家激活时针对单个会员卡分配自定义的会员卡背景。  可为空
        /// </summary>
        public string background_pic_url { get; set; }
        /// <summary>
        /// 需要设置的积分全量值，传入的数值会直接显示   
        /// </summary>
        public string bonus { get; set; }
        /// <summary>
        /// 商家自定义积分消耗记录，不超过14个汉字。       
        /// </summary>
        public string record_bonus { get; set; }
        /// <summary>
        /// 需要设置的余额全量值，传入的数值会直接显示
        /// </summary>
        public string balance { get; set; }
        /// <summary>
        /// 商家自定义金额消耗记录，不超过14个汉字。         
        /// </summary>
        public string record_balance { get; set; }
        /// <summary>
        /// 创建时字段custom_field1定义类型的最新数值，限制为4个汉字，12字节。 
        /// </summary>
        public string custom_field_value1 { get; set; }
        /// <summary>
        /// 创建时字段custom_field2定义类型的最新数值，限制为4个汉字，12字节。 
        /// </summary>
        public string custom_field_value2 { get; set; }
        /// <summary>
        /// 创建时字段custom_field3定义类型的最新数值，限制为4个汉字，12字节。 
        /// </summary>
        public string custom_field_value3{ get; set; }
        /// <summary>
        /// 控制原生消息结构体，包含各字段的消息控制字段
        /// </summary>
        public NotifyOptional notify_optional { get; set; }
        /// <summary>
        /// 控制原生消息结构体
        /// </summary>
        public class NotifyOptional
        {
            public bool? is_notify_bonus { get; set; }
            public bool? is_notify_balance { get; set; }
            public bool? is_notify_custom_field1 { get; set; }
            public bool? is_notify_custom_field2 { get; set; }
            public bool? is_notify_custom_field3 { get; set; }
        }
    }
}
