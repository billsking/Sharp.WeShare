﻿namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 开通券点账户时，返回的实体
    /// </summary>
    public class Reward:BaseRes
    {
        /// <summary>
        /// 奖励券点数量，以元为单位，微信卡券对每一个新开通券点账户的商户奖励200个券点
        /// </summary>
        public int reward { get; set; }
    }
}