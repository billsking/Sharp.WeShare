﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
   public class MemberCard : BaseCard
    {
        public MemberCard()
        {
            this.card_type = "MEMBER_CARD";
        }

        /// <summary>
        /// 会员卡信息
        /// </summary>
        public CardInfo member_card { get; set; }
    }
}
