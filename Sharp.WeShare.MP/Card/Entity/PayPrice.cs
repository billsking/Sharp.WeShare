﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 调用对优惠券批价接口，返回的实体
    /// </summary>
    public class PayPrice : BaseRes
    {
        /// <summary>
        /// 本次批价的订单号，用于下面的确认充值库存接口，仅对当前订单有效且仅可以使用一次，60s内可用于兑换库存。
        /// </summary>
        public string order_id { get; set; }
        /// <summary>
        /// 本次需要支付的券点总额度
        /// </summary>
        public decimal price { get; set; }
        /// <summary>
        /// 本次需要支付的免费券点额度
        /// </summary>
        public decimal free_coin { get; set; }
        /// <summary>
        /// 本次需要支付的付费券点额度
        /// </summary>
        public decimal pay_coin { get; set; }
    }
}
