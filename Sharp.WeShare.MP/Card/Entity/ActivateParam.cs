﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 会员卡激活参数
    /// </summary>
    public class ActivateParam
    {
        /// <summary>
        ///  会员卡编号，由开发者填入，作为序列号显示在用户的卡包里。可与Code码保持等值。          
        /// </summary>
        public string membership_number { get; set; }
        /// <summary>
        /// 创建会员卡时获取的初始code。      
        /// </summary>
        public string code { get; set; }
        /// <summary>
        /// 卡券id,自定义code卡券必填
        /// </summary>
        public string card_id { get; set; }
        /// <summary>
        /// 商家自定义会员卡背景图，须
        ///先调用上传图片接口将背景图上传至cdn，否则报错，卡面设计请遵循微信会员卡自定义背景设计规范
        /// </summary>
        public string background_pic_url { get; set; }
        /// <summary>
        /// 激活后的有效起始时间。若不填写默认以创建时的 data_info 为准。unix时间戳格式。 
        /// </summary>
        public int activate_begin_time { get; set; }
        /// <summary>
        /// 激活后的有效截至时间。若不填写默认以创建时的 data_info 为准。unix时间戳格式。
        /// </summary>
        public int activate_end_time { get; set; }
        /// <summary>
        /// 初始积分
        /// </summary>
        public int init_bonus { get; set; }
        /// <summary>
        /// 初始余额，不填为0。 
        /// </summary>
        public string init_balance { get; set; }
        /// <summary>
        /// 创建时字段custom_field1定义类型的初始值，限制为4个汉字，12字节。
        /// </summary>
        public string init_custom_field_value1 { get; set; }
        /// <summary>
        /// 创建时字段custom_field2定义类型的初始值，限制为4个汉字，12字节。
        /// </summary>
        public string init_custom_field_value2 { get; set; }
        /// <summary>
        /// 创建时字段custom_field3定义类型的初始值，限制为4个汉字，12字节。
        /// </summary>
        public string init_custom_field_value3 { get; set; }
    }
}
