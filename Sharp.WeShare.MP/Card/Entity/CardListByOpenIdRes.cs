﻿using System.Collections.Generic;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 指定用户所有可用卡券列表及未失效卡券列表。
    /// </summary>
    public class CardListByOpenIdRes:BaseRes
    {
        /// <summary>
        /// 是否有可用的朋友的券
        /// </summary>
        public bool has_share_card { get; set; }
        /// <summary>
        /// 卡券列表
        /// </summary>
        public List<CardInfo> card_list { get; set; }
        public class CardInfo
        {
            public string code { get; set; }
            public string card_id { get; set; }
        }
    }
}
