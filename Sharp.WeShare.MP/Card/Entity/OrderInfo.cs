﻿using Sharp.WeShare.MP.EnumKey;

namespace Sharp.WeShare.MP.Card.Entity
{
    /// <summary>
    /// 充值点券，订单详情。
    /// </summary>
    public class OrderInfo:BaseRes
    {
        public string order_id { get; set; }
        /// <summary>
        /// 订单状态
        /// </summary>
        public PayCoinStatus status { get; set; }
        /// <summary>
        /// 订单创建时间
        /// </summary>
        public int create_time { get; set; }
        /// <summary>
        /// 支付完成时间
        /// </summary>
        public int pay_finish_time { get; set; }
        /// <summary>
        /// 支付描述，一般为微信支付充值
        /// </summary>
        public string desc { get; set; }
        /// <summary>
        /// 本次充值的免费券点数量，以元为单位
        /// </summary>
        public int free_coin_count { get; set; }
        /// <summary>
        /// 本次充值的付费券点数量，以元为单位
        /// </summary>
        public int pay_coin_count { get; set; }
        /// <summary>
        /// 	回退的免费券点
        /// </summary>
        public int refund_free_coin_count { get; set; }
        /// <summary>
        /// 回退的付费券点
        /// </summary>
        public int refund_pay_coin_count { get; set; }
        /// <summary>
        /// 支付人的openid
        /// </summary>
        public string openid { get; set; }
        /// <summary>
        /// 订单类型，ORDER_TYPE_WXPAY为充值
        /// </summary>
        public string order_tpye { get; set; }
    }
}