﻿using Sharp.WeShare.MP.EnumKey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Card.Entity
{
    public class TimeLimit
    {
        /// <summary>
        /// 使用时段限制,此处只控制显示， 不控制实际使用逻辑，不填默认不显示
        /// </summary>
        public WeekType type { get; set; }
        /// <summary>
        /// 当前type类型下的起始时间（小时）
        /// </summary>
        public int begin_hour { get; set; }
        /// <summary>
        /// 当前type类型下的起始时间（分钟）
        /// </summary>
        public int begin_minute { get; set; }
        /// <summary>
        /// 当前type类型下的结束时间（小时）
        /// </summary>
        public int end_hour { get; set; }
        /// <summary>
        /// 当前type类型下的结束时间（分钟）
        /// </summary>
        public int end_minute { get; set; }

    }
}
