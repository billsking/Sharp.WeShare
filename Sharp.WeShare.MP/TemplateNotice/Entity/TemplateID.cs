﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.TemplateNotice.Entity
{
    public class TemplateID : BaseRes
    {
        /// <summary>
        /// 模板ID
        /// </summary>
        public string template_id { get; set; }
    }
}
