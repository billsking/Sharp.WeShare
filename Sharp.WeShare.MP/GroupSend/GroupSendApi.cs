﻿using Sharp.WeShare.Common;
using Sharp.WeShare.MP.GroupSend.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.GroupSend
{
    public class GroupSendApi
    {

        /// <summary>
        /// 上传视频素材
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="media_id"></param>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static UploadRes UpLoadVideo(string accessToken,string media_id, string title,string description)
        {
            var url =
            string.Format("https://file.api.weixin.qq.com/cgi-bin/media/uploadvideo?access_token={0}", accessToken);
            var json = new
            {
                media_id = media_id,
                title = title,
                description = description
            };
            return Utils.PostResult<UploadRes>(json, url);
        }

        /// <summary>
        /// 基础发送接口
        /// </summary>
        /// <param name="obj">json对象</param>
        /// <param name="accessToken">accessToken</param>
        /// <param name="sendtype">群发类型，1为按分组群发，2为按openid列表群发，3为
        ///预览接口。默认为按分组群发</param>
        private static SendRes BaseSend(string accessToken, object obj,  int
        sendtype = 1)
        {
            string url = null;
            switch (sendtype)
            {
                //分组群发
                case 1:
                    url =
          string.Format("https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token={0}", accessToken); break;
                //openid列表群发
                case 2:
                    url =
          string.Format("https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token={0}", accessToken); break;
                //预览接口
                case 3:
                    url =
          string.Format("https://api.weixin.qq.com/cgi-bin/message/mass/preview?access_token={0}", accessToken); break;
                default:
                    url = string.Format("https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token={0}", accessToken); break;
            }
            return Utils.PostResult<SendRes>(obj, url);
        }

        /// <summary>
        ///  按分组群发图文消息，isall为true时，说明群发所有用户，此时group_id可为空； 否则，根据group_id进行群发
        /// </summary>
        /// <param name="accessToken">accessToken</param>
        /// <param name="media_id">图片的媒体ID</param>
        /// <param name="isall">是否群发所有用户</param>
        /// <param name="group_id">分组ID</param>
        public static SendRes SendArticleByGroup(string accessToken, string media_id, bool isall = true, string group_id = "")
        {
            var json = new
            {
                filter = new
                {
                    is_to_all = isall,
                    group_id = group_id
                },
                mpnews = new
                {
                    media_id = media_id
                },
                msgtype = "mpnews"
            };
            return BaseSend(accessToken,json);
        }

        public static BaseRes SendTextByGroup(string accessToken, string content, bool isall = true, string group_id = "")
        {
            var json = new
            {
                filter = new
                {
                    is_to_all = isall,
                    group_id = group_id
                },
                text = new
                {
                    content = content
                },
                msgtype = "text"
            };
            return BaseSend( accessToken,json);
        }
        /// <summary>
        /// 根据分组群发卡券
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="accessToken"></param>
        /// <param name="isall"></param>
        /// <param name="group_id"></param>
        /// <returns></returns>
        public static BaseRes SendCardByGroup( string accessToken,string cardId, bool isall = true, string group_id = "")
        {
            var json = new
            {
                filter = new
                {
                    is_to_all = isall,
                    group_id = group_id
                },
                wxcard = new
                {
                    card_id = cardId
                },
                msgtype = "wxcard"
            };
            return BaseSend(accessToken,json);
        }
        public static BaseRes SendVoiceByGroup(string accessToken,string media_id,  bool isall = true, string group_id = "")
        {
            var json = new
            {
                filter = new
                {
                    is_to_all = isall,
                    group_id = group_id
                },
                voice = new
                {
                    media_id = media_id
                },
                msgtype = "voice"
            };
            return BaseSend(accessToken,json);
        }
        public static BaseRes SendImgByGroup(string accessToken,string media_id,  bool isall = true, string group_id = "")
        {
            var json = new
            {
                filter = new
                {
                    is_to_all = isall,
                    group_id = group_id
                },
                image = new
                {
                    media_id = media_id
                },
                msgtype = "image"
            };
            return BaseSend(accessToken,json,  1);
        }
        public static BaseRes SendVideoByGroup(string accessToken,string media_id,  bool isall = true, string group_id = "")
        {
            var json = new
            {
                filter = new
                {
                    is_to_all = isall,
                    group_id = group_id
                },
                mpvideo = new
                {
                    media_id = media_id
                },
                msgtype = "mpvideo"
            };
            return BaseSend( accessToken,json);
        }

        #region 按openid列表群发和预览接口“订阅号不可用，服务号认证后可用”
        /// <summary>
        /// 按用户列表群发文本消息
        /// </summary>
        /// <param name="content">群发内容</param>
        /// <param name="accessToken">accessToken</param>
        /// <param name="touser">如果为数组，则表示openid列表，调用的是群发接口；否则表 示openid，调用的是预览接口</param>
        public static BaseRes SendTextByOpenID(string
        accessToken,string content,  object touser)
        {
            var json = new
            {
                touser = touser,
                text = new
                {
                    content = content
                },
                msgtype = "text"
            };
            return BaseSend(accessToken,json,  touser.GetType().IsArray ? 2 : 3);
        }

        /// <summary>
        /// 根据openid列表群发卡券
        /// </summary>
        /// <param name="cardId">卡券编号</param>
        /// <param name="accessToken"></param>
        /// <param name="touser">如果为数组，则表示openid列表，调用的是群发接口；否则表 示openid，调用的是预览接口</param>
        /// <returns></returns>
        public static BaseRes SendCardByOpenID(string  accessToken, string cardId, object touser)
        {
            var json = new
            {
                touser = touser,
                wxcard = new
                {
                    card_id = cardId
                },
                msgtype = "wxcard"
            };
            return BaseSend(accessToken, json, touser.GetType().IsArray ? 2 : 3);
        }
        /// <summary>
        /// 按用户列表群发图片消息
        /// </summary>
        /// <param name="media_id">图片媒体ID</param>
        /// <param name="accessToken">accessToken</param>
        /// <param name="touser">如果为数组，则表示openid列表，调用的群发接口；否则表示openid，调用的是预览接口</param>
        public static BaseRes SendImgByOpenID( string accessToken, string media_id,object touser)
        {
            var json = new
            {
                touser = touser,
                image = new
                {
                    media_id = media_id
                },
                msgtype = "image"
            };
            return BaseSend(accessToken, json, touser.GetType().IsArray ? 2 : 3);
        }
        /// <summary>
        /// 按用户列表群发图片消息
        /// </summary>
        /// <param name="media_id">语音媒体ID</param>
        /// <param name="accessToken">accessToken</param>
        /// <param name="touser">如果为数组，则表示openid列表，调用的是群发接口；否则表
        ///示openid，调用的是预览接口</param>
        public static BaseRes SendVoiceByOpenID(string accessToken,string media_id,  object touser)
        {
            var json = new
            {
                touser = touser,
                voice = new
                {
                    media_id = media_id
                },
                msgtype = "voice"
            };
            return BaseSend(accessToken, json, touser.GetType().IsArray ? 2 : 3);
        }
        /// <summary>
        /// 按用户列表群发图文消息
        /// </summary>
        /// <param name="media_id">图文ID</param>
        /// <param name="accessToken">accessToken</param>
        /// <param name="touser">如果为数组，则表示openid列表，调用的是群发接口；否则表
        ///示openid，调用的是预览接口</param>
        public static BaseRes SendArticleByOpenID(string  accessToken,string media_id,  object touser)
        {
            var json = new
            {
                touser = touser,
                mpnews = new
                {
                    media_id = media_id
                },
                msgtype = "mpnews"
            };
            return BaseSend(accessToken, json, touser.GetType().IsArray ? 2 : 3);
        }
        /// <summary>
        /// 按用户列表群发视频消息
        /// </summary>
        /// <param name="media_id">视频ID</param>
        /// <param name="accessToken">accessToken</param>
        /// <param name="touser">如果为数组，则表示openid列表，调用的群是发接口；否则表
        ///示openid，调用的是预览接口</param>
        public static BaseRes SendVideoByOpenID( string  accessToken, string media_id,object touser)
        {
            var json = new
            {
                touser = touser,
                mpvideo = new
                {
                    media_id = media_id
                },
                msgtype = "mpvideo"
            };
            return BaseSend(accessToken,json,  touser.GetType().IsArray ? 2 : 3);
        }
        #endregion

        /// <summary>
        /// 查询群发状态
        /// </summary>
        /// <param name="msg_id"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static GroupStatus QueryStatus(string accessToken,string msg_id)
        {
            var url =
       string.Format("https://api.weixin.qq.com/cgi-bin/message/mass/get?access_token={0}", accessToken);
            var json = new { msg_id = msg_id };
            return Utils.PostResult<GroupStatus>(json, url);
        }
        /// <summary>
        /// 删除群发
        /// </summary>
        /// <param name="msg_id">群发消息ID</param>
        public static BaseRes Delete(string accessToken,string msg_id)
        {
            var url =
            string.Format("https://api.weixin.qq.com/cgi-bin/message/mass/delete?access_token={0}", accessToken);
            var json = new { msg_id = msg_id };
            return Utils.PostResult<BaseRes>(json, url);
        }

    }
}
