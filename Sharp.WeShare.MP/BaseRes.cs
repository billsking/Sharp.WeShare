﻿namespace Sharp.WeShare.MP
{
    /// <summary>
    /// 接口响应的基类
    /// </summary>
    public class BaseRes
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public BaseRes()
        {
            errmsg = "success";
        }
    }
}