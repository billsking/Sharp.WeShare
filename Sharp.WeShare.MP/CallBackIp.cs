﻿using Sharp.WeShare.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP
{
    public class CallBackIp : BaseRes
    {
        public string[] ip_list { get; set; }
        private static string[] _ip_list;
        private static DateTime exptime;
        /// <summary>
        /// 获取微信服务器ip
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static string[] GetList(string accessToken)
        {
            if (_ip_list == null || exptime < DateTime.Now)
            {
                var url = string.Format("https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token={0}", accessToken);
                var ret = Utils.GetResult<CallBackIp>(url);
                if (ret.errcode == 0)
                {
                    _ip_list = ret.ip_list;
                    exptime = DateTime.Now.AddDays(1);
                    return ret.ip_list;
                }
            }
            return _ip_list;
        }
    }
}
