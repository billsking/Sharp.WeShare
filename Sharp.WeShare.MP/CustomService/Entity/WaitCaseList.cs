﻿using System;
using System.Collections.Generic;
using Sharp.WeShare.Common;

namespace Sharp.WeShare.MP.CustomService.Entity
{
    public class WaitCaseList:BaseRes
    {
        /// <summary>
        /// 未接入会话列表，最多返回100条数据，按照来访顺序
        /// </summary>
        public List<waitinfo> waitcaselist { get; set; }
        public int count { get; set; }
        public class waitinfo 
        {
            /// <summary>
            /// 粉丝的最后一条消息的时间
            /// </summary>
            public string latest_time { get; set; }
            public string openid { get; set; }

            public DateTime JoinTime
            {
                get { return Utils.UnixTimeToTime(latest_time); }
            }
        }
    }
}