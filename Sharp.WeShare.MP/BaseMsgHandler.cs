﻿using System;
using System.Web;
using Sharp.WeShare.Common;
using Sharp.WeShare.MP.MsgEntity;

namespace Sharp.WeShare.MP
{
    public class BaseMsgHandler : IHttpHandler, IMsgHandler
    {
        public virtual bool IsReusable
        {
            get { return false; }
        }


        public EnterParam param;
        public virtual void ProcessRequest(HttpContext context)
        {
            //验证请求的合法性
            if (!Utils.VaildUrl(param.token))
            {
                context.Response.Write("非法请求");
                return;
            }
            if (context.Request.HttpMethod.ToLower() == "get")
            {
                //get请求，表示的是开发者接入流程，验证url成功后,需返回echostr
                context.Response.Write(context.Request.QueryString["echostr"]);
            }
            else
            {
                //绑定消息与处理事件
                BaseMsg.BindEvent(this, param);
            }
        }

        public virtual void TextHandler(TextMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void ImageHandler(ImgMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void VoiceHandler(VoiceMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void VideoHandler(VideoMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void LocationHandler(LocationMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void LinkHandler(LinkMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void ScanHandler(ScanQrEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void SubscribeHandler(SubscribeEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void UnSubscribeHandler(EventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void MenuViewHandler(ViewEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void LocationEventHandler(LocationEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void CommonHandler(BaseMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void VerifyHandler(VerifyEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void UserPayHandler(UserPayEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void UserGetCardHandler(UserGetCardEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void PoiNotifyHandler(PoiNotifyEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void SubmitMemberUserInfoHandler(SubmitMemberUserInfoEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void MenuClickHandler(BaseMenuEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void MenuScanPushHandler(ScanMenuEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void MenuScanWaitHandler(ScanMenuEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void MenuPicHandler(PicMenuEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void MenuLocationHandler(LocationMenuEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void GroupSendJobHandler(GroupSendJobEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }

        public virtual void TemplateJobEventHandler(TemplateJobEventMsg msg)
        {
            HttpContext.Current.Response.Write("");
        }
    }
}