﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.MaterialLib.Entity
{
    /// <summary>
    /// 图文内图片上传
    /// </summary>
   public class NewImgUrlRes
    {
        public string url { get; set; }
    }
}
