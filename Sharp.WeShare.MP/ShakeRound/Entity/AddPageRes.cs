﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.ShakeRound.Entity
{
    public class AddPageRes:BaseRes
    {
        public d data { get; set; }
        public class d{
            public int page_id { get; set; }
        }
    }
}
