﻿using Sharp.WeShare.MP.EnumKey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.Menu.Entity
{
    /// <summary>
    /// 菜单信息类
    /// </summary>
    public class ButtonInfo
    {
        public MenuType  type { get; set; }
        public string name { get; set; }
        public string key { get; set; }
        public string url { get; set; }
        public string media_id { get; set; }
        /// <summary>
        /// 子菜单
        /// </summary>
        public List<ButtonInfo> sub_button { get; set; }
    }
}
