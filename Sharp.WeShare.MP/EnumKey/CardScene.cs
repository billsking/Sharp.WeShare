﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.EnumKey
{
    /// <summary>
    /// 卡券货架场景
    /// </summary>
    public enum CardScene
    {
        /// <summary>
        /// 附近
        /// </summary>
        SCENE_NEAR_BY,
        /// <summary>
        /// 自定义菜单
        /// </summary>
        SCENE_MENU,
        /// <summary>
        /// 二维码
        /// </summary>
        SCENE_QRCODE,
        /// <summary>
        /// 公众号文章
        /// </summary>
        SCENE_ARTICLE,
        /// <summary>
        /// h5页面
        /// </summary>
        SCENE_H5,
        /// <summary>
        ///自动回复
        /// </summary>
        SCENE_IVR,
        /// <summary>
        ///卡券自定义cell
        /// </summary>
        SCENE_CARD_CUSTOM_CELL
    }
}
