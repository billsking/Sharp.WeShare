﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.EnumKey
{
    /// <summary>
    /// 素材类型
    /// </summary>
  public  enum MaterialType
    {
        image,
        voice,
        video,
        thumb
    }
}
