﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.EnumKey
{
    public enum CardColor
    {
        Color010,
        Color020,
        Color030,
        Color040,
        Color050,
        Color060,
        Color070,
        Color080,
        Color090,
        Color100,
        Color101,
        Color102
    }
}
