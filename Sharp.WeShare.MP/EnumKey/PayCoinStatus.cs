﻿namespace Sharp.WeShare.MP.EnumKey
{
    /// <summary>
    /// 充值点券订单状态
    /// </summary>
    public enum PayCoinStatus
    {
        /// <summary>
        /// 等待支付
        /// </summary>
        ORDER_STATUS_WAITING,
        /// <summary>
        /// 支付成功
        /// </summary>
        ORDER_STATUS_SUCC,
        /// <summary>
        /// 加代币成功
        /// </summary>
        ORDER_STATUS_FINANCE_SUCC,
        /// <summary>
        /// 加库存成功
        /// </summary>
        ORDER_STATUS_QUANTITY_SUCC,
        /// <summary>
        /// 已退币
        /// </summary>
        ORDER_STATUS_HAS_REFUND,
        /// <summary>
        /// 等待退币确认
        /// </summary>
        ORDER_STATUS_REFUND_WAITING,
        /// <summary>
        /// 已回退, 系统失败
        /// </summary>
        ORDER_STATUS_ROLLBACK,
        /// <summary>
        /// 已开发票
        /// </summary>
        ORDER_STATUS_HAS_RECEIPT
    }
}