﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.EnumKey
{
    /// <summary>
    /// 会员卡类目
    /// </summary>
    public enum MemberCardType
    {
        /// <summary>
        /// 等级
        /// </summary>
        FIELD_NAME_TYPE_LEVEL,
        /// <summary>
        /// 优惠券
        /// </summary>
        FIELD_NAME_TYPE_COUPON,
        /// <summary>
        /// 印花
        /// </summary>
        FIELD_NAME_TYPE_STAMP,
        /// <summary>
        /// 折扣
        /// </summary>
        FIELD_NAME_TYPE_DISCOUNT,
        /// <summary>
        /// 成就
        /// </summary>
        FIELD_NAME_TYPE_ACHIEVEMEN,
        /// <summary>
        /// 里程
        /// </summary>
        FIELD_NAME_TYPE_MILEAGE
    }
}
