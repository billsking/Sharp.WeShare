﻿namespace Sharp.WeShare.MP.EnumKey
{
    public enum SortType
    {
        /// <summary>
        /// 升序
        /// </summary>
        SORT_ASC,
        /// <summary>
        /// 降序
        /// </summary>
        SORT_DESC
    }
}