﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.MP.EnumKey
{
    /// <summary>
    /// 卡券状态
    /// </summary>
    public enum CardStatus
    {
        /// <summary>
        /// 待审核
        /// </summary>
        CARD_STATUS_NOT_VERIFY,
        /// <summary>
        /// 审核失败
        /// </summary>
        CARD_STATUS_VERIFY_FAIL,
        /// <summary>
        /// 通过审核
        /// </summary>
        CARD_STATUS_VERIFY_OK,
        /// <summary>
        /// 卡券被商户删除
        /// </summary>
        CARD_STATUS_DELETE,
        /// <summary>
        /// 在公众平台投放过的卡券
        /// </summary>
        CARD_STATUS_DISPATCH
    }
}
