﻿namespace Sharp.WeShare.MP.UserManager.Entity
{
    /// <summary>
    /// 批量获取用户基本信息时，提交的实体类。
    /// </summary>
    public class Openid
    {
        public string openid { get; set; }
        public string lang { get; set; }

        public Openid()
        {
            lang = "zh-CN";
        }
    }
}