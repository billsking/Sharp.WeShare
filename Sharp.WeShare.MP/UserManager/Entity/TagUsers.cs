﻿using System.Collections.Generic;

namespace Sharp.WeShare.MP.UserManager.Entity
{
    /// <summary>
    /// 标签下的用户列表
    /// </summary>
   public class TagUsers:BaseRes
    {
        public int count { get; set; }
        public string next_openid { get; set; }
        public List<string> openid { get; set; }
       
    }
}
