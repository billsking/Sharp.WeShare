﻿using Sharp.WeShare.MP;
using Sharp.WeShare.MP.Card;
using Sharp.WeShare.MP.Card.Entity;
using Sharp.WeShare.MP.EnumKey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sharp.WeShare.Common;

namespace MpTest
{
    public partial class CardTest : System.Web.UI.Page
    {
        string accesstoken = AccessToken.GetValue(WxConfig.APPID,WxConfig.APPSECRET);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var imgurl = CardApi.UploadImgUrl(accesstoken, MapPath("/11.jpg"));
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            var colors = CardApi.GetColors(accesstoken);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            var card = new Cash
            {
                cash = new CardInfo
                {
                    reduce_cost = 100,
                    least_cost = 0,
                    base_info = new BaseInfo
                    {
                        brand_name = "腾讯",
                        bind_openid = false,
                        can_give_friend = true,
                        can_share = true,
                        //center_sub_title = "赶紧使用吧",
                        //center_title = "立即使用",
                        center_url = "http://www.baidu.com",
                        code_type = Sharp.WeShare.MP.EnumKey.CodeType.CODE_TYPE_NONE,
                        color = "Color010",
                        custom_url = "http://www.baidu.com",
                        custom_url_name = "更多惊喜",
                        custom_url_sub_title = "快来看满屋",
                        date_info = new DateInfo
                        {
                            type = Sharp.WeShare.MP.EnumKey.CardDateType.DATE_TYPE_FIX_TERM,
                            fixed_term = 1
                        },
                        description = "我是描述",
                        get_limit = 1,
                        logo_url = "https://mmbiz.qlogo.cn/mmbiz/icIxxjVX9Taahjvl2gAPMyYBKyXCzLcg2tjyfrIssVa0AwuQYX26dAsgLRgM0Nib1yxKrrqJ4WQMuyapFmicooY1w/0?wx_fmt=png",
                        notice = "请出示二维码",
                        service_phone = "18745784512",
                        sku = new Sku { quantity = 0 },
                        source = "张思凯",
                        sub_title = "测试优惠券，请勿外传",
                        title = "代金券50",
                        location_id_list = new[] { "463033483", "219366766" },
                        use_custom_code = true,
                         get_custom_code_mode= "GET_CUSTOM_CODE_MODE_DEPOSIT",
                    },
                    advanced_info = new AdvancedInfo
                    {
                        @abstract = new Abstract
                        {
                            @abstract = "我是摘要",
                            icon_url_list = new string[] { "https://mmbiz.qlogo.cn/mmbiz/icIxxjVX9Taahjvl2gAPMyYBKyXCzLcg2tjyfrIssVa0AwuQYX26dAsgLRgM0Nib1yxKrrqJ4WQMuyapFmicooY1w/0?wx_fmt=png" }
                        },
                        business_service = new List<BusinessServiceType>
                           {
                                BusinessServiceType.BIZ_SERVICE_DELIVER,
                                BusinessServiceType.BIZ_SERVICE_FREE_PARK,
                                BusinessServiceType.BIZ_SERVICE_FREE_WIFI,
                                BusinessServiceType.BIZ_SERVICE_WITH_PET
                           },
                        text_image_list = new List<TextImage>
                         {
                              new TextImage { text="文本", image_url="https://mmbiz.qlogo.cn/mmbiz/icIxxjVX9Taahjvl2gAPMyYBKyXCzLcg2tjyfrIssVa0AwuQYX26dAsgLRgM0Nib1yxKrrqJ4WQMuyapFmicooY1w/0?wx_fmt=png" }
                         },
                        time_limit = new List<TimeLimit>
                          {
                               new TimeLimit { type = WeekType.SATURDAY, begin_hour=10, begin_minute=10, end_hour =20, end_minute=20 }
                          },
                        use_condition = new UseCondition
                        {
                            accept_category = "冷饮",
                            can_use_with_other_discount = true,
                        }
                    }
                }
                

            };
            var id = CardApi.Create(accesstoken, card);
            //预存模式卡券:pR19CsxVpaDleqp319-ssnLXwcoc
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            var card = new Cash
            {
                cash = new CardInfo
                {
                    base_info = new BaseInfo
                    {
                        brand_name = "腾讯",
                        use_all_locations = true,
                        location_id_list = new[] { "220766284" },
                        can_give_friend = false,
                        can_share = false,
                        center_sub_title = "赶紧使用吧",
                        center_title = "立即使用",
                        center_url = "http://www.baidu.com",
                        code_type = Sharp.WeShare.MP.EnumKey.CodeType.CODE_TYPE_BARCODE,
                        color = "Color010",
                        custom_url = "http://www.baidu.com",
                        custom_url_name = "更多惊喜",
                        custom_url_sub_title = "快来看满屋",
                        date_info = new DateInfo
                        {
                            type = Sharp.WeShare.MP.EnumKey.CardDateType.DATE_TYPE_FIX_TIME_RANGE,
                            begin_timestamp = Utils.GetTimeStamp(),
                            end_timestamp = Utils.ConvertDateTimeInt(DateTime.Now.AddDays(10))
                        },
                        description = "我是描述",
                        get_limit = 10,
                        logo_url = "https://mmbiz.qlogo.cn/mmbiz/icIxxjVX9Taahjvl2gAPMyYBKyXCzLcg2tjyfrIssVa0AwuQYX26dAsgLRgM0Nib1yxKrrqJ4WQMuyapFmicooY1w/0?wx_fmt=png",
                        notice = "请出示二维码",
                        service_phone = "18745784512",
                      
                    },
                    advanced_info = new AdvancedInfo
                    {
                        @abstract = new Abstract
                        {
                            @abstract = "我是摘要",
                            icon_url_list = new string[] { "https://mmbiz.qlogo.cn/mmbiz/icIxxjVX9Taahjvl2gAPMyYBKyXCzLcg2tjyfrIssVa0AwuQYX26dAsgLRgM0Nib1yxKrrqJ4WQMuyapFmicooY1w/0?wx_fmt=png" }
                        },
                        business_service = new List<BusinessServiceType>
                           {
                                BusinessServiceType.BIZ_SERVICE_DELIVER,
                                BusinessServiceType.BIZ_SERVICE_FREE_PARK,
                                BusinessServiceType.BIZ_SERVICE_FREE_WIFI,
                                BusinessServiceType.BIZ_SERVICE_WITH_PET
                           },
                        text_image_list = new List<TextImage>
                         {
                              new TextImage { text="文本", image_url="https://mmbiz.qlogo.cn/mmbiz/icIxxjVX9Taahjvl2gAPMyYBKyXCzLcg2tjyfrIssVa0AwuQYX26dAsgLRgM0Nib1yxKrrqJ4WQMuyapFmicooY1w/0?wx_fmt=png" }
                         },
                        time_limit = new List<TimeLimit>
                          {
                               new TimeLimit { type = WeekType.SATURDAY, begin_hour=10, begin_minute=10, end_hour =20, end_minute=20 }
                          },
                        use_condition = new UseCondition
                        {
                            accept_category = "冷饮",
                            reject_category = "冰淇淋",
                            can_use_with_other_discount = true,
                        },
                        share_friends = true,
                        consume_share_self_num = 1,
                    }
                    ,
                    reduce_cost = 20,
                }
            };
            var id = CardApi.Create(accesstoken, card);
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            var res = CardApi.ActivateCoinAccount(accesstoken);
            Response.Write(res.reward);
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            //p8mXIt6yBLyGtjQc7StNXhTE_2MI
            //
            //首先查下价格
            var res = CardApi.GetPayPrice(accesstoken, "pR19Cs0b99ATenA5pbLJ7NESODfA", 1);
            var a = CardApi.ConfirmBuy(accesstoken, "pR19Cs0b99ATenA5pbLJ7NESODfA", res.order_id, 1);
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            var res = CardApi.GetCoinsInfo(accesstoken);
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            var res = CardApi.Recharge(accesstoken, 10);
        }

        protected void Button9_Click(object sender, EventArgs e)
        {
            var res = CardApi.SetPayCell(accesstoken, "pR19Cs5Vhza93rN6DDGRPwEojQOI", true);
        }

        protected void Button10_Click(object sender, EventArgs e)
        {
            var res = CardApi.SetSelfConsume(accesstoken, "pR19Cs32ZNHuvpxgyFLtlrxFA1QQ", true,true, true);
        }

        protected void Button11_Click(object sender, EventArgs e)
        {
            var res = CardApi.CreateQr(accesstoken, new SingleCardParam { card_id = "pR19Cs1fduyJ5khBpwCZ32ARYT3s", outer_str = "222"});
        }

        protected void Button12_Click(object sender, EventArgs e)
        {
            var page = new LandingPage
            {
                banner = "http://mmbiz.qpic.cn/mmbiz/hY2dXTibb9fNNYzMK4Gemjlpl4kGKpLpgyYp6YN5aRQqpdicqlUB8NFXPo3jcNM7OVG7y8oDo3Nia7FyyNQvoO4kA/640?wx_fmt=jpeg&tp=webp&wxfrom=5",
                can_share = true,
                page_title = "卡券领取",
                scene = CardScene.SCENE_ARTICLE,
                card_list = new List<LandingPage.CardInfo>
                     {
                         new LandingPage.CardInfo
                         {
                              card_id="pR19Cs7XRlLLckjpsXqlgiQOX0QU",
                               thumb_url="https://mmbiz.qlogo.cn/mmbiz/icIxxjVX9Taa5kDAibRnNAzohquZ4kxL79GwCGyB2cHiaVib4Nobh03aMHII2icQLzagrV0H1lbZaHcWto3Ayiaia2Pyw/0?wx_fmt=jpeg"
                         },
                         new LandingPage.CardInfo
                         {
                              card_id="pR19CsxVpaDleqp319-ssnLXwcoc",
                               thumb_url="https://mmbiz.qlogo.cn/mmbiz/icIxxjVX9Taa5kDAibRnNAzohquZ4kxL79GwCGyB2cHiaVib4Nobh03aMHII2icQLzagrV0H1lbZaHcWto3Ayiaia2Pyw/0?wx_fmt=jpeg"
                         }
                     }
            };
            var res = CardApi.CreatePage(accesstoken, page);
        }

        protected void Button13_Click(object sender, EventArgs e)
        {
            var res = CardApi.ImportCode(accesstoken, "pR19Cs1fduyJ5khBpwCZ32ARYT3s", new[] {
                "123","456","789","abc"
            }.ToList());
        var res1 =    CardApi.UpdateStock(accesstoken, "pR19Cs1fduyJ5khBpwCZ32ARYT3s", 4);

        }

        protected void Button14_Click(object sender, EventArgs e)
        {
            var res = CardApi.QueryCodeCount(accesstoken, "pR19CsxVpaDleqp319-ssnLXwcoc");
        }

        protected void Button15_Click(object sender, EventArgs e)
        {
            var res = CardApi.CheckCode(accesstoken, "pR19CsxVpaDleqp319-ssnLXwcoc", new[] {
                "123","456","789","abc"
            }.ToList());
        }

        protected void Button16_Click(object sender, EventArgs e)
        {
            var res = CardApi.GetHtml(accesstoken, "pR19CsxVpaDleqp319-ssnLXwcoc");
        }

        protected void Button17_Click(object sender, EventArgs e)
        {
            var res = CardApi.QueryCode(accesstoken, "014958726541");
        }

        protected void Button18_Click(object sender, EventArgs e)
        {
            var res = CardApi.GetCardInfo(accesstoken, "pR19CsxVpaDleqp319-ssnLXwcoc");
        }
    }
}