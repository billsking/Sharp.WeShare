﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Sharp.WeShare.Applet.Login;
using Sharp.WeShare.Common;

namespace MpTest
{
    /// <summary>
    /// ApplitHandler 的摘要说明
    /// </summary>
    public class ApplitHandler : IHttpHandler
    {
        private HttpContext _context;
        public string Appid = "wxe6f18184a1050058";
        public string Secret = "bac698054ac6173950f5ee0bab40f8a7";
        public void ProcessRequest(HttpContext context)
        {
            this._context = context;
            var action = context.Request.QueryString["action"];
            switch (action)
            {
                case "login"://登录的code换取session
                    Login();
                    break;
            }
        }

        private void Login()
        {
            var code = _context.Request.Form["code"];
            if (!string.IsNullOrEmpty(code))
            {
                var mysession = LoginApi.CodeToMySession(Appid, Secret, code);
                _context.Response.Write(JsonConvert.SerializeObject(new
                {
                    status = mysession.errcode,
                    session = mysession.errcode == 0 ? mysession.MyKey : mysession.errmsg
                }));
                return;
            }
            _context.Response.Write(JsonConvert.SerializeObject(new
            {
                status =-1,
                msg="code为空"
            }));

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}