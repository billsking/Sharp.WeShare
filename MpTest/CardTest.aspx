﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CardTest.aspx.cs" Inherits="MpTest.CardTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="上传卡券所需图片" />
        <br />
        <br />
        <asp:Button ID="Button2" runat="server" Text="获取卡券颜色列表" OnClick="Button2_Click" />
    
        <br />
        <br />
        <br />
        <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="创建卡券" />
    
        <br />
        <br />
        <asp:Button ID="Button4" runat="server" OnClick="Button4_Click" Text="创建朋友的券" />
    
        <br />
        <br />
        <br />
        <asp:Button ID="Button5" runat="server" OnClick="Button5_Click" Text="开通券点账户" />
        <br />
        <br />
        <asp:Button ID="Button6" runat="server" OnClick="Button6_Click" Text="添加朋友的券库存" />
    
        <br />
        <br />
        <br />
        <asp:Button ID="Button7" runat="server" OnClick="Button7_Click" Text="查询余额" />
        <br />
        <br />
        <br />
        <asp:Button ID="Button8" runat="server" OnClick="Button8_Click" Text="充值点券" />
    
        <br />
        <br />
        <asp:Button ID="Button9" runat="server" OnClick="Button9_Click" Text="设置买单" />
        <br />
        <br />
        <br />
        <asp:Button ID="Button10" runat="server" OnClick="Button10_Click" Text="设置自助核销" />
    
        <br />
        <br />
        <asp:Button ID="Button11" runat="server" OnClick="Button11_Click" Text="创建单卡券二维码" />
    
        <br />
        <asp:Button ID="Button12" runat="server" Text="创建卡券货架"  OnClick="Button12_Click"/>
        <br />
        <br />
        <asp:Button ID="Button13" runat="server" OnClick="Button13_Click" Text="导入code" />
        <br />
        <br />
        <asp:Button ID="Button14" runat="server" OnClick="Button14_Click" Text="查询导入的数量" />
        <br />
        <br />
        <asp:Button ID="Button15" runat="server" OnClick="Button15_Click" Text="检查导入的code" />
        <br />
    
        <br />
        <br />
        <asp:Button ID="Button16" runat="server" OnClick="Button16_Click" Text="获取嵌入图文中的卡券代码" />
        <br />
        <br />
        <asp:Button ID="Button17" runat="server" OnClick="Button17_Click" Text="查询code" />
    
        <br />
        <br />
        <br />
        <asp:Button ID="Button18" runat="server" OnClick="Button18_Click" Text="获取卡券信息" />
    
    </div>
    </form>
</body>
</html>
