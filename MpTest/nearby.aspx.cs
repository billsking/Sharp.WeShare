﻿using Sharp.WeShare.MP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sharp.WeShare.MP.ShakeRound;
using Sharp.WeShare.MP.ShakeRound.Entity;

namespace MpTest
{
    public partial class nearby : System.Web.UI.Page
    {
        string accesstoken = AccessToken.GetValue(WxConfig.APPID, WxConfig.APPSECRET);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var upres = ShakeRoundApi.UploadMaterial(accesstoken, MapPath("111.png"));
            var res = ShakeRoundApi.AddPage(accesstoken, new PageInfo
            {
                 comment="我这个是测试页面",
                  description="我是副标题",
                   icon_url= upres.data.pic_url,
                    page_url ="http://fanyuip.iego.cn/round.aspx",
                    title="我是标题"
            });
        }
    }
}