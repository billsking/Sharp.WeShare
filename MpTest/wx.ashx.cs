﻿using Sharp.WeShare.MP.MsgEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using Sharp.WeShare.Common;
using Sharp.WeShare.MP;
using Sharp.WeShare.MP.CustomService;

namespace MpTest
{
    public class wx : BaseMsgHandler
    {

        public override void ProcessRequest(HttpContext context)
        {
            param = new EnterParam
            {
                appid = WxConfig.APPID,
                EncodingAESKey = WxConfig.ENCODINGAESKEY,
                token = WxConfig.TOKEN
            };
            base.ProcessRequest(context);
        }

        public override void TextHandler(TextMsg msg)
        {
            msg.ResponseTxt(msg.Content, param);
        }

        public override void LocationHandler(LocationMsg msg)
        {
            
        }

        public override void UserPayHandler(UserPayEventMsg msg)
        {
            
        }
        public override void MenuClickHandler(BaseMenuEventMsg msg)
        {
            msg.ResponseTxt(msg.EventKey,param);
        }

        public override void MenuViewHandler(ViewEventMsg msg)
        {
            // msg.ResponseTxt(msg.EventKey, param);  跳转链接的按钮，无法通过此方法回复消息
            //使用客服接口回复
            CustomServiceApi.SendText(AccessToken.GetValue(WxConfig.APPID,WxConfig.APPSECRET), msg.FromUserName, msg.EventKey);

        }
        public override void MenuScanPushHandler(ScanMenuEventMsg msg)
        {
            CustomServiceApi.SendText(AccessToken.GetValue(WxConfig.APPID, WxConfig.APPSECRET), msg.FromUserName, msg.ScanCodeInfo.ScanType);
        }
        public override bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}