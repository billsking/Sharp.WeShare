﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.Pay
{
    public enum TradeType
    {
        /// <summary>
        /// 公众号内支付
        /// </summary>
        JSAPI,
        /// <summary>
        /// 扫码支付
        /// </summary>
        NATIVE,
        /// <summary>
        /// app支付
        /// </summary>
        APP,
        /// <summary>
        /// H5支付
        /// </summary>
        MWEB,
        /// <summary>
        /// 被扫支付
        /// </summary>
        MICROPAY
    }
}
