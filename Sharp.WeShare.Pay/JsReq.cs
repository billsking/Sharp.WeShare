﻿namespace Sharp.WeShare.Pay
{
    /// <summary>
    /// 小程序或者微信公众号中发起支付的参数
    /// </summary>
    public class JsReq
    {
        public string appId { get; set; }
        public string timeStamp { get; set; }
        public string nonceStr { get; set; }
        public string package { get; set; }
        public string signType { get; set; } = "MD5";
        public string paySign { get; set; }
    }
}