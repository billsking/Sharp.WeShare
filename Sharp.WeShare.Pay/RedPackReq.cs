﻿using Sharp.WeShare.Common;

namespace Sharp.WeShare.Pay
{
    public class RedPackReq
    {
        /// <summary>
        /// 随机字符串 
        /// </summary>
        public string nonce_str { get; set; }
        /// <summary>
        /// 签名 
        /// </summary>
        public string sign { get; set; }
        /// <summary>
        /// 商户订单号 
        /// </summary>
        public string mch_billno { get; set; }
        /// <summary>
        /// 商户号 
        /// </summary>
        public string mch_id { get; set; }
        /// <summary>
        /// 公众账号AppID
        /// </summary>
        public string wxappid { get; set; }
        ///// <summary>
        ///// 提供方名称  
        ///// </summary>
        //public string nick_name { get; set; }
        /// <summary>
        /// 商户名称  
        /// </summary>
        public string send_name { get; set; }
        /// <summary>
        /// 用户openid
        /// </summary>
        public string re_openid { get; set; }
        /// <summary>
        /// 付款金额 
        /// </summary>
        public int total_amount { get; set; }
        /// <summary>
        /// 红包发放总人数 
        /// </summary>
        public int total_num { get; set; }=1;
        /// <summary>
        /// 红包祝福语  
        /// </summary>
        public string wishing { get; set; }
        /// <summary>
        /// IP地址 
        /// </summary>
        public string client_ip { get; set; }
        /// <summary>
        /// 活动名称 
        /// </summary>
        public string act_name { get; set; }
        /// <summary>
        /// 备注 
        /// </summary>
        public string remark { get; set; }
        ///// <summary>
        ///// 商户logo的URL 
        ///// </summary>
        //public string logo_imgurl { get; set; }
        ///// <summary>
        ///// 分享文案  
        ///// </summary>
        //public string share_content { get; set; }
        ///// <summary>
        ///// 分享链接  
        ///// </summary>
        //public string share_url { get; set; }
        ///// <summary>
        ///// 分享的图片 
        ///// </summary>
        //public string share_imgurl { get; set; }

        public RedPackRes PayRequest(string key, string certpath , string certpwd) 
        {
            var param = Utils.EntityToDictionary(this);//将实体转换成数据集合
            param.Add("sign", Utils.GetPaySign(param, key));//生成签名，并将签名添加到数据集
            var xml = Utils.parseXML(param);//将数据集合转换成XML
            var res = Utils.HttpPost("https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack", xml, certpath, certpwd);
            return XmlToEntity<RedPackRes>(res, key);
        }
        private static T XmlToEntity<T>(string xml, string key) where T : BasePayRes, new()
        {
            var dic = Utils.XmlToDictionary(xml);
            if (dic["return_code"] == "FAIL" || dic["result_code"] == "FAIL")
            {
                if (dic.ContainsKey("result_code"))
                {
                    return new T
                    {
                        return_code = dic["return_code"],
                        result_code = dic["result_code"],
                        err_code = dic["err_code"],
                        err_code_des = dic["err_code_des"]

                    };
                }
                return new T
                {
                    return_code = dic["return_code"]
                };
            }
            if (dic.ContainsKey("sign"))
            {
                var originalsign = dic["sign"];
                dic.Remove("sign");
                var sign = Utils.GetPaySign(dic, key);
                if (sign == originalsign)
                {
                    dic.Add("sign", sign);
                }
                else
                {
                    return new T { err_code = "自定义错误", err_code_des = "微信返回的参数签名校验失败，数据可能被篡改，原始数据为：" + xml };
                }
            }
            return Utils.DictionaryToObject<T>(dic);
        }
    }
}
