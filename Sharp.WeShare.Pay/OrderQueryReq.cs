﻿namespace Sharp.WeShare.Pay
{
    /// <summary>
    /// 订单查询实体
    /// </summary>
    public class OrderQueryReq : BasePay
    {
        /// <summary>
        /// 微信的订单号，优先使用
        /// </summary>
        public string transaction_id { get; set; }
        /// <summary>
        /// 商户系统内部的订单号。transaction_id、 out_trade_no 二选一。如果同时存在，则优先级如下：transaction_id> out_trade_no
        /// </summary>
        public string out_trade_no { get; set; }
        public string sign { get; set; }
    }
}
