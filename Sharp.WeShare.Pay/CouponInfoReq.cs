﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sharp.WeShare.Pay
{
    public class CouponInfoReq : BasePay
    {
        /// <summary>
        /// 代金券id
        /// </summary>
        public string coupon_id { get; set; }
        /// <summary>
        /// 用户标识
        /// </summary>
        public string openid { get; set; }
        /// <summary>
        /// 代金劵对应的批次号
        /// </summary>
        public string stock_id { get; set; }
        /// <summary>
        /// 设备号
        /// </summary>
        public string device_info { get; set; }
        /// <summary>
        /// 操作员
        /// </summary>
        public string op_user_id { get; set; }
        /// <summary>
        /// 协议版本. 默认1.0 
        /// </summary>
        public string version { get; set; } = "1.0";
        /// <summary>
        ///协议类型 XML【目前仅支持默认XML】 
        /// </summary>
        public string type { get; set; } = "XML";

    }
}
