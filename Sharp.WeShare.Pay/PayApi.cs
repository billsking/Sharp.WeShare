﻿using System;
using System.Collections.Generic;
using System.Web;
using Sharp.WeShare.Common;

namespace Sharp.WeShare.Pay
{
    public class PayApi
    {
        /// <summary>
        /// 统一支付下单接口
        /// </summary>
        /// <param name="req">支付请求的参数</param>
        /// <param name="key">支付密钥</param>
        /// <returns></returns>
        public static UnifiedRes UnifiedOrder(UnifiedReq req, string key)
        {
            var url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
            return req.PayRequest<UnifiedRes>(url, key);
        }
        /// <summary>
        /// 扫码支付模式二
        /// </summary>
        /// <param name="param"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static UnifiedRes ScanCodePayTwo(ScanCodeTwoParam param,string key)
        {
            var req = new UnifiedReq
            {
                appid = param.appid,
                attach = param.attach,
                body = param.body,
                detail = param.detail,
                device_info = param.device_info,
                fee_type = param.fee_type,
                total_fee = param.total_fee,
                goods_tag = param.goods_tag,
                limit_pay = param.limit_pay,
                mch_id = param.mch_id,
                notify_url = param.notify_url,
                out_trade_no = param.out_trade_no,
                product_id = param.product_id,
                spbill_create_ip = param.spbill_create_ip,
                time_expire = param.time_expire,
                time_start = param.time_start,
                trade_type = TradeType.NATIVE,
            };
            return UnifiedOrder(req, key);
        }
        /// <summary>
        /// 查询订单信息
        /// </summary>
        /// <param name="req">支付请求的参数</param>
        /// <param name="key">支付密钥</param>
        /// <returns></returns>
        public static OrderQueryRes QueryOrder(OrderQueryReq req, string key)
        {
            var url = "https://api.mch.weixin.qq.com/pay/orderquery";
            return req.PayRequest<OrderQueryRes>(url, key);
        }
        /// <summary>
        /// 关闭订单.商户订单支付失败需要生成新单号重新发起支付，要对原订单号调用关单，避免重复支付；系统下单后，用户支付超时，系统退出不再受理，避免用户继续，请调用关单接口。
        ///注意：订单生成后不能马上调用关单接口，最短调用时间间隔为5分钟。
        /// </summary>
        /// <param name="req">支付请求的参数</param>
        /// <param name="key">支付密钥</param>
        /// <returns></returns>
        public static CloseOrderRes CloseOrder(CloseOrderRes req, string key)
        {
            var url = "https://api.mch.weixin.qq.com/pay/closeorder";
            return req.PayRequest<CloseOrderRes>(url, key);
        }
        /// <summary>
        /// 发放代金券
        /// </summary>
        /// <param name="req">支付请求的参数</param>
        /// <param name="key">支付密钥</param>
        /// <param name="cert">证书目录</param>
        /// <param name="pwd">证书密码</param>
        /// <returns></returns>
        public static CouponRes SendCoupon(CouponReq req, string key,string cert,string pwd)
        {
            var url = "https://api.mch.weixin.qq.com/mmpaymkttransfers/send_coupon";
            return req.PayRequest<CouponRes>(url, key,cert,pwd);
        }
        public static RedPackRes SendRedPack(RedPackReq req, string key, string cert, string pwd)
        {
            return req.PayRequest(key, cert, pwd);
        }
        /// <summary>
        /// 查询代金券批次信息
        /// </summary>
        /// <param name="req"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static CouponStockRes QueryCouponStock(CouponStockReq req, string key)
        {
            var url = "https://api.mch.weixin.qq.com/mmpaymkttransfers/query_coupon_stock";
            return req.PayRequest<CouponStockRes>(url, key);
        }
        /// <summary>
        /// 查询代金券信息
        /// </summary>
        /// <param name="req"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static CouponInfoRes QueryCouponInfo(CouponInfoReq req, string key)
        {
            var url = "https://api.mch.weixin.qq.com/mmpaymkttransfers/querycouponsinfo";
            return req.PayRequest<CouponInfoRes>(url, key);
        }

        public static void GetNotifyRes(string key, Action<OrderInfo> action)
        {
            try
            {
                var reqdata = Utils.GetRequestData();
                var rev = Utils.XmlToObject<OrderInfo>(reqdata);
                if (rev.return_code != "SUCCESS")
                { BackMessage("通信错误"); return; }
                if (rev.result_code != "SUCCESS")
                { BackMessage("业务出错"); return; }
                if (rev.sign == Utils.GetPaySign(rev, key))
                {
                    //回调函数，业务逻辑处理，处理结束后返回信息给微信
                    action(rev);
                }
            }
            catch (Exception e)
            {
                BackMessage("回调函数处理错误");
            }
        }
        public static void BackMessage(string msg = "")
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            if (msg != "")
            {
                dic.Add("return_code", "FAIL");
                dic.Add("return_msg", msg);
                HttpContext.Current.Response.Write(Utils.parseXML(dic));
            }
            else
            {
                dic.Add("return_code", "SUCCESS");
                HttpContext.Current.Response.Write(Utils.parseXML(dic));
            }

        }

    }
}