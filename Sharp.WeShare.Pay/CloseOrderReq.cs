﻿namespace Sharp.WeShare.Pay
{
    public class CloseOrderReq : BasePay
    {
        /// <summary>
        /// 商户系统内部的订单号
        /// </summary>
        public string out_trade_no { get; set; }
        public string sign { get; set; }
    }
}
