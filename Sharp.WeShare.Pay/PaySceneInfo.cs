﻿namespace Sharp.WeShare.Pay
{
    public class PaySceneInfo
    {
        public H5Info h5_info { get; set; }
        public class H5Info
        {
            /// <summary>
            /// 场景类型h5支付固定传"h5_info" 
            /// </summary>
            public string type { get; set; } 
            /// <summary>
            /// 应用名 。android或者ios专用
            /// </summary>
            public string app_name { get; set; }
            /// <summary>
            /// 包名 android专用
            /// </summary>
            public string package_name { get; set; }
            /// <summary>
            /// bundle_id ios专用
            /// </summary>
            public string bundle_id { get; set; }
            /// <summary>
            /// WAP网站URL地址
            /// </summary>
            public string wap_url { get; set; }
            /// <summary>
            /// WAP 网站名
            /// </summary>
            public string wap_name { get; set; }
        }
    }
}